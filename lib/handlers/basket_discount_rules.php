<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;
use Bitrix\Catalog;
    cModule::IncludeModule('catalog');
    class CSaleAdditionalCtrl extends CGlobalCondCtrlComplex
    {
        public static function GetClassName()
        {
            return __CLASS__;
        }

        public static function GetControlDescr(){
            $arResult = array();

            $strClassName = static::GetClassName();
            $arControls = static::GetControls();
            foreach ($arControls as &$arOneControl)
            {
                $arResult[] = array(
                    'ID' => $arOneControl['ID'],
                    'GROUP' => 'Y',
                    'GetControlShow' => array($strClassName, 'GetControlShow'),
                    'GetConditionShow' => array($strClassName, 'GetConditionShow'),
                    'IsGroup' => array($strClassName, 'IsGroup'),
                    'Parse' => array($strClassName, 'Parse'),
                    'Generate' => array($strClassName, 'Generate'),
                    'ApplyValues' => array($strClassName, 'ApplyValues'),
                    'InitParams' => array($strClassName, 'InitParams')
                );
            }

            if (isset($arOneControl))
                unset($arOneControl);
            return $arResult;
        }

        public static function GetControlShow($arParams)
        {
            $arControls = static::GetControls();
            $arResult = array(
                'controlgroup' => true,
                'group' =>  false,
                'label' => 'Спец. условия',
                'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                'children' => array()
            );
            foreach ($arControls as &$arOneControl)
            {
                $arResult['children'][] = array(
                    'controlId' => $arOneControl['ID'],
                    'group' => false,
                    'label' => $arOneControl['LABEL'],
                    'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                    'control' => array(
                        $arOneControl['LABEL'],
                        static::GetLogicAtom($arOneControl['LOGIC']),
                        static::GetValueAtom($arOneControl['JS_VALUE'])
                    )
                );
            }
            if (isset($arOneControl))
                unset($arOneControl);

            return $arResult;
        }

        /**
         * @param bool|string $strControlID
         * @return array|bool
         */
        public static function GetControls($strControlID = false)
        {

            $arLabels = array(
                BT_COND_LOGIC_EQ => GetMessage('BT_SALE_AMOUNT_LOGIC_EQ_LABEL'),
                BT_COND_LOGIC_NOT_EQ => GetMessage('BT_SALE_AMOUNT_LOGIC_NOT_EQ_LABEL'),
                BT_COND_LOGIC_GR => GetMessage('BT_SALE_AMOUNT_LOGIC_GR_LABEL'),
                BT_COND_LOGIC_LS => GetMessage('BT_SALE_AMOUNT_LOGIC_LS_LABEL'),
                BT_COND_LOGIC_EGR => GetMessage('BT_SALE_AMOUNT_LOGIC_EGR_LABEL'),
                BT_COND_LOGIC_ELS => GetMessage('BT_SALE_AMOUNT_LOGIC_ELS_LABEL')
            );
            $arControlList = array(
                'CondSaleOrderCount' => array(
                    'ID' => 'CondSaleOrderCount',
                    'FIELD' => 'ORDER_COUNT',
                    'MODULE_ID' => false,
                    'MODULE_ENTITY' => false,
                    'MULTIPLE' => 'N',
                    'EXECUTE_MODULE' => 'sale',
                    'FIELD_TYPE' => 'int',
                    'LABEL' => 'Количество заказов пользователя',
                    'LOGIC' => static::GetLogicEx($arLabels),
                    'JS_VALUE' => array(
                        'type' => 'input'
                    )
                )
            );

            if (false === $strControlID)
            {
                return $arControlList;
            }
            elseif (isset($arControlList[$strControlID]))
            {
                return $arControlList[$strControlID];
            }
            else
            {
                return false;
            }
        }

        public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = false)
        {

            $strResult = '';
            if (is_string($arControl))
            {
                $arControl = static::GetControls($arControl);
            }

            $boolError = !is_array($arControl);

            if (!$boolError)
            {
                $arValues = static::Check($arOneCondition, $arOneCondition, $arControl, false);
                $boolError = ($arValues === false);
            }

            if (!$boolError)
            {
                switch ($arControl['ID'])
                {
                    case 'CondSaleOrderCount':
                        $strResult =  self::__GetOrderCountCond($arOneCondition, $arValues, $arParams, $arControl, $arSubs);
                        break;
                }
            }
            return (!$boolError ? $strResult : false);
        }

        private function __GetOrderCountCond($arOneCondition, $arValues, $arParams, $arControl, $arSubs)
        {
            $arLogic = static::SearchLogic($arValues['logic'], $arControl['LOGIC']);
            if (!isset($arLogic['OP'][$arControl['MULTIPLE']]) || empty($arLogic['OP'][$arControl['MULTIPLE']]))
            {
                $boolError = true;
            }
            else
            {
                $boolMulti = false;
                if (isset($arControl['JS_VALUE']['multiple']) && 'Y' == $arControl['JS_VALUE']['multiple'])
                {
                    $boolMulti = true;
                }
                $orderCount = "CL_Users::GetCountOrder(\$arOrder)";
                if (!$boolMulti)
                {
                    $strResult = str_replace(array('#FIELD#', '#VALUE#'), array($orderCount, $arValues['value']), $arLogic['OP'][$arControl['MULTIPLE']]);
                }
                else
                {
                    $arResult = array();
                    foreach ($arValues['value'] as &$mxValue)
                    {
                        $arResult[] = str_replace(array('#FIELD#', '#VALUE#'), array($orderCount, $mxValue), $arLogic['OP'][$arControl['MULTIPLE']]);
                    }
                    if (isset($mxValue))
                        unset($mxValue);
                    $strResult = '(('.implode(') || (', $arResult).'))';
                }
            }
            return $strResult;
        }
    }

    class CCatalogCondCtrlIBlockPropsAsia extends  CCatalogCondCtrlIBlockProps{
        public static function GetClassName()
        {
            return __CLASS__;
        }

        public static function GetLogic($arOperators = false)
        {
            $arOperatorsList = array(
                'PHP_CODE' => array(
                    'ID' => 'PHP_CODE',
                    'OP' => array(
                        'N' => '#VALUE#',
                        'Y' => '#VALUE#'
                    ),
                    'PARENT' => ' && ',
                    'MULTI_SEP' => ' && ',
                    'VALUE' => 'PhpCode',
                    'LABEL' => 'PHP код'
                )
            );

            $boolSearch = false;
            $arSearch = array();
            if (!empty($arOperators) && is_array($arOperators))
            {
                foreach ($arOperators as &$intOneOp)
                {
                    if (isset($arOperatorsList[$intOneOp]))
                    {
                        $boolSearch = true;
                        $arSearch[$intOneOp] = $arOperatorsList[$intOneOp];
                    }
                }
                unset($intOneOp);
            }
            return ($boolSearch ? $arSearch : $arOperatorsList);
        }

        public static function GetControls($strControlID = false)
        {
            $arControlList = array();
            $arIBlockList = array();
            $rsIBlocks = CCatalog::GetList(array(), array(), false, false, array('IBLOCK_ID', 'PRODUCT_IBLOCK_ID'));
            while ($arIBlock = $rsIBlocks->Fetch())
            {
                $arIBlock['IBLOCK_ID'] = (int)$arIBlock['IBLOCK_ID'];
                $arIBlock['PRODUCT_IBLOCK_ID'] = (int)$arIBlock['PRODUCT_IBLOCK_ID'];
                if ($arIBlock['IBLOCK_ID'] > 0)
                    $arIBlockList[$arIBlock['IBLOCK_ID']] = true;
                if ($arIBlock['PRODUCT_IBLOCK_ID'] > 0)
                    $arIBlockList[$arIBlock['PRODUCT_IBLOCK_ID']] = true;
            }
            unset($arIBlock, $rsIBlocks);
            if (!empty($arIBlockList))
            {
                $arIBlockList = array_keys($arIBlockList);
                sort($arIBlockList);
                foreach ($arIBlockList as &$intIBlockID)
                {
                    $strName = CIBlock::GetArrayByID($intIBlockID, 'NAME');
                    if (false !== $strName)
                    {
                        $boolSep = true;
                        $rsProps = CIBlockProperty::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'), array('IBLOCK_ID' => $intIBlockID));
                        while ($arProp = $rsProps->Fetch())
                        {
                            if ('CML2_LINK' == $arProp['XML_ID'] || 'F' == $arProp['PROPERTY_TYPE'])
                                continue;

                            $strFieldType = '';
                            $arLogic = array();
                            $arValue = array();
                            $arPhpValue = '';

                            $arLogic = array();
                            $boolUserType = false;

                            $strFieldType = 'text';
                            $arLogic = static::GetLogic(array('PHP_CODE'));
                            $arValue = array(
                                'type' => 'input',
                                'format' => 'text'
                            );

                            $arControlList['CondIBPropPHP:'.$intIBlockID.':'.$arProp['ID']] = array(
                                'ID' => 'CondIBPropPHP:'.$intIBlockID.':'.$arProp['ID'],
                                'PARENT' => false,
                                'EXIST_HANDLER' => 'Y',
                                'MODULE_ID' => 'catalog',
                                'MODULE_ENTITY' => 'iblock',
                                'ENTITY' => 'ELEMENT_PROPERTY',
                                'IBLOCK_ID' => $intIBlockID,
                                'FIELD' => 'PROPERTY_'.$arProp['ID'].'_VALUE',
                                'FIELD_TABLE' => $intIBlockID.':'.$arProp['ID'],
                                'FIELD_TYPE' => $strFieldType,
                                'MULTIPLE' => 'Y',
                                'GROUP' => 'N',
                                'SEP' => ($boolSep ? 'Y' : 'N'),
                                'SEP_LABEL' => ($boolSep ? str_replace(array('#ID#', '#NAME#'), array($intIBlockID, $strName), Loc::getMessage('BT_MOD_CATALOG_COND_CMP_CATALOG_PROP_LABEL')) : '') . ' (выражение PHP)',
                                'LABEL' => $arProp['NAME'],
                                'PREFIX' => str_replace(array('#NAME#', '#IBLOCK_ID#', '#IBLOCK_NAME#'), array($arProp['NAME'], $intIBlockID, $strName), Loc::getMessage('BT_MOD_CATALOG_COND_CMP_CATALOG_ONE_PROP_PREFIX')) . ' (выражение PHP)',
                                'LOGIC' => $arLogic,
                                'JS_VALUE' => $arValue,
                                'PHP_VALUE' => $arPhpValue
                            );
                            $boolSep = false;
                        }
                    }
                }
                if (isset($intIBlockID))
                    unset($intIBlockID);
                unset($arIBlockList);
            }

            if ($strControlID === false)
            {
                return $arControlList;
            }
            elseif (isset($arControlList[$strControlID]))
            {
                return $arControlList[$strControlID];
            }
            else
            {
                return false;
            }
        }

        public static function GetControlShow($arParams)
        {
            $arControls = static::GetControls();

            $arResult = array();
            $intCount = -1;
            foreach ($arControls as &$arOneControl)
            {
                if (isset($arOneControl['SEP']) && 'Y' == $arOneControl['SEP'])
                {
                    $intCount++;
                    $arResult[$intCount] = array(
                        'controlgroup' => true,
                        'group' =>  false,
                        'label' => $arOneControl['SEP_LABEL'],
                        'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                        'children' => array()
                    );
                }
                $arLogic = static::GetLogicAtom($arOneControl['LOGIC']);
                $arValue = static::GetValueAtom($arOneControl['JS_VALUE']);

                $arResult[$intCount]['children'][] = array(
                    'controlId' => $arOneControl['ID'],
                    'group' => false,
                    'label' => $arOneControl['LABEL'],
                    'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                    'control' => array(
                        array(
                            'id' => 'prefix',
                            'type' => 'prefix',
                            'text' => $arOneControl['PREFIX']
                        ),
                        $arLogic,
                        $arValue
                    )
                );
            }
            if (isset($arOneControl))
                unset($arOneControl);

            return $arResult;
        }

        public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = false)
        {
            $strResult = '';

            if (is_string($arControl))
            {
                $arControl = static::GetControls($arControl);
            }
            $boolError = !is_array($arControl);

            if (!$boolError)
            {
                $strResult = parent::Generate($arOneCondition, $arParams, $arControl, $arSubs);

                if (false === $strResult || '' == $strResult)
                {
                    $boolError = true;
                }
                else
                {
                    $strField = 'isset('.$arParams['FIELD'].'[\''.$arControl['FIELD'].'\'])';
                    $strResult = $strField.' && '.$strResult;
                }
            }

            $strResult = str_replace('#VALUE#', $arParams['FIELD'] . '[\'' . $arControl['FIELD'] .'\']',  $strResult);
            $strResult = str_replace('"', '', $strResult);
            return (!$boolError ? $strResult : false);
        }

    }

    AddEventHandler("sale", "OnCondSaleControlBuildList", Array("CSaleAdditionalCtrl", "GetControlDescr"));
    AddEventHandler("catalog", "OnCondCatControlBuildList", Array("CCatalogCondCtrlIBlockPropsAsia", "GetControlDescr"));